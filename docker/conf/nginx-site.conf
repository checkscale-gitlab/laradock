server {
	listen   80;
	listen   [::]:80 default ipv6only=on;

    root /var/www/html/public;
    index index.php /_h5ai/public/index.php;
    # server_name example.com;

	location / {
        autoindex on;
        disable_symlinks off;
        try_files $uri $uri/ /index.php$is_args$args;
        add_header Access-Control-Allow-Origin "*";
	}
    
	location ~ \.php$ {
        try_files $uri =404;
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php-fpm.sock;
		fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param SCRIPT_NAME $fastcgi_script_name;
		fastcgi_index index.php;
		include fastcgi_params;
	}

	location ~* \.(jpg|jpeg|gif|png|css|js|ico|webp|tiff|ttf|svg)$ {
		expires           5d;
	}

	location ^~ /.well-known/acme-challenge/ {
        root  /var/www/letsencrypt;
        allow all;
    }



    # This URL provides RTMP statistics in XML
    location /stat {
        rtmp_stat all;
        rtmp_stat_stylesheet stat.xsl;
    }

    location /stat.xsl {
        root /var/www;
    }

    # reates the http-location full-resolution HLS stream - "http://example.com/live/my-stream-key/index.m3u8"      
    location /live {
        autoindex on;
        alias /tmp/hls/live;
        add_header Cache-Control no-cache;

        # CORS setup
        add_header 'Access-Control-Allow-Origin' '*' always;
        add_header 'Access-Control-Expose-Headers' 'Content-Length';
        add_header 'Access-Control-Expose-Headers' 'Range';

        # allow CORS preflight requests
        if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Max-Age' 1728000;
            add_header 'Content-Type' 'text/plain charset=UTF-8';
            add_header 'Content-Length' 0;
            return 204;
        }

        types {
            application/vnd.apple.mpegurl m3u8;
            video/mp2t ts;
	    } 
    }
    #creates the http-location full-resolution HLS stream - "http://example.com/show/my-stream-key/index.m3u8"      
    location /show {
        autoindex on;
        alias /tmp/hls/show;
        add_header Cache-Control no-cache;

        # CORS setup
        add_header 'Access-Control-Allow-Origin' '*' always;
        add_header 'Access-Control-Expose-Headers' 'Content-Length';
        add_header 'Access-Control-Expose-Headers' 'Range';

        # allow CORS preflight requests
        if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Max-Age' 1728000;
            add_header 'Content-Type' 'text/plain charset=UTF-8';
            add_header 'Content-Length' 0;
            return 204;
        }

        types {
            application/vnd.apple.mpegurl m3u8;
            video/mp2t ts;
	    } 

    }
    
    location /vod {
        autoindex on;
        alias /tmp/hls/vod_publish;
        add_header Cache-Control no-cache;

        # CORS setup
        add_header 'Access-Control-Allow-Origin' '*' always;
        add_header 'Access-Control-Expose-Headers' 'Content-Length';
        add_header 'Access-Control-Expose-Headers' 'Range';

        # allow CORS preflight requests
        if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Max-Age' 1728000;
            add_header 'Content-Type' 'text/plain charset=UTF-8';
            add_header 'Content-Length' 0;
            return 204;
        }

        types {
            application/vnd.apple.mpegurl m3u8;
            video/mp2t ts;
	    } 

    }

}
