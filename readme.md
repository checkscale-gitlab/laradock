<p align="center"><img src="https://i.imgur.com/LTJyLKy.png" height="100%" width="400" ></p>

# Laravel LEMP stack for docker

This package lets you easily start new Laravel projects with docker.

Includes:
 * Composer
 * FFmpeg
 * LetsEncrypt
 * NPM
 * RTMP streaming module.
 * Supervisord

## Installation

```bash
git clone git@gitlab.com:nomercy_entertainment/nomercy-mediaserver.git laradocklemp
cd laradocklemp
```

##### Copy the .env.example to .env file end edit it before continuing.

## Build the docker stack.
```bash
docker-compose up -d
```

## Usage
The ./src folder is the mounted as the web root.

Create a new laravel project:
```bash
composer global require laravel/installer
composer create-project --prefer-dist laravel/laravel ./src
```

Clone a laravel project:
```bash 
git clone git@gitlab.com:nomercy_entertainment/nomercy-mediaserver.git ./src
cd src
```
##### Copy the .env.example to .env file end edit it before continuing.

## Using LetsEncrypt
##### Make sure you've set the DOMAIN variable.
```cmd 
docker exec -it nginx-php /usr/bin/letsencrypt-setup
```

## Sreaming and recording:

You can stream with any rtmp stream software to rtmp://example.com/live/\<name\>\
It wil by default re-encode the stream to:
* source
* 1080p/60 8000kbps
* 1080p/30 6000kbps
* 720p/60  6000kbps
* 720p/30  4000kbps

##### The original livestream is automatically recorded.

The urls for the playlist files can be found at:
*       http(s)://example.com/show/<name>.m3u8
                                 <name>_1080p/index.m3u8
                                 <name>_720p/index.m3u8
                                 <name>_audio/index.m3u8

Restreaming to YouTube or Twitch can be done by setting the streaming keys and matching the restream name with the stream name in the .env file.


## Contributing
Pull requests are welcome.

## License
[gpl-3.0](https://choosealicense.com/licenses/gpl-3.0/)
